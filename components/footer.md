Source: [git.logikum.hu] ♦ 2017-{{ #current-year }} Logikum, Ltd.
<br />
<i><small>
If you find bugs or have feature requests, please [let us know].
</small></i>
