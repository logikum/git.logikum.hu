|||
|-------------------|----------------------------------------------------------|
| [feature graph]   | Illustrate the flows of __feature__ subcommands. |
| [bugfix graph]    | Illustrate the flows of __bugfix__ subcommands. |
| [release graph]   | Illustrate the flows of __release__ subcommands. |
| [hotfix graph]    | Illustrate the flows of __hotfix__ subcommands. |
| [support graph]   | Illustrate the flows of __support__ subcommands. |
