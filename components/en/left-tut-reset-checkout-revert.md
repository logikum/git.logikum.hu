#### reset, checkout, revert

* [overview](/tutorials/reset-checkout-revert/overview)
* commit-level operations
  * [reset](/tutorials/reset-checkout-revert/cl-reset)
  * [checkout](/tutorials/reset-checkout-revert/cl-checkout)
  * [revert](/tutorials/reset-checkout-revert/cl-revert)
* file-level operations
  * [reset](/tutorials/reset-checkout-revert/fl-reset)
  * [checkout](/tutorials/reset-checkout-revert/fl-checkout)
* [summary](/tutorials/reset-checkout-revert/summary)
