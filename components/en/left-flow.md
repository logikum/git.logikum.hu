#### subcommands

* [init]
* [feature]
* [bugfix]
* [release]
* [hotfix]
* [support]
* [config]
* [log]
* [version]

#### flow graphs

* [feature graph]
* [bugfix graph]
* [release graph]
* [hotfix graph]
* [support graph]
