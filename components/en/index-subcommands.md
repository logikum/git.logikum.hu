|||
|-------------|-----------------------------------------------------------------|
| [init]      | Initialize a new git repo with support for the branching model. |
| [feature]   | Manage your feature branches. |
| [bugfix]    | Manage your bugfix branches. |
| [release]   | Manage your release branches. |
| [hotfix]    | Manage your hotfix branches. |
| [support]   | Manage your support branches. |
| [config]    | Manage your git-flow configuration. |
| [log]       | Show log deviating from base branch. |
| [version]   | Shows version information. |
