<!-- ======================================================================
--- Search engine
title:          git flows
keywords:       AVH edition
description:    The subcommands of GitFlow AVH Edition.
--- Menu system
order:          20
text:           Flow
hidden:         false
umbel:          true
--- Page properties
id:             
document:       
layout:         
searchable:     false
---rewrite:        /flow/index
======================================================================= -->_

# GitFlow AVH Edition

Version: 1.11.0

Usage:

```text
git flow <subcommand>
```

Available subcommands are:

{{ =index-subcommands }}

The original article of the branching modle by Vincent Driessen:
[http://nvie.com/posts/a-successful-git-branching-model/](http://nvie.com/posts/a-successful-git-branching-model/ "|")

A quick cheatsheet made by Daniel Kummer:
[http://danielkummer.github.io/git-flow-cheatsheet/](http://danielkummer.github.io/git-flow-cheatsheet/ "|")
