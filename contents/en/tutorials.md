<!-- ======================================================================
--- Search engine
title:          git tutorials
keywords:       tutorials
description:    These articles provide an in-depth discussion of git features.
--- Menu system
order:          10
text:           Tutorials
hidden:         false
umbel:          true
--- Page properties
id:             
document:       
layout:         
searchable:     false
======================================================================= -->_

# Git tutorials

Each of these articles provide an in-depth discussion of an advanced feature of
Git. Instead of presenting new commands and concepts, they refine your existing
Git skills by explaining what’s going on under the hood. Armed with this knowledge,
you’ll be able to use familiar Git commands more effectively.

### Revisions

<i>Source: [Reference Manual](https://git-scm.com/docs/gitrevisions "|")</i>

Many Git commands take revision parameters as arguments. Depending on the command,
they denote a specific commit or, for commands which walk the revision graph (such
as `git log`), all commits which are reachable from that commit. For commands that
walk the revision graph one can also specify a range of revisions explicitly.

In addition, some Git commands (such as `git show`) also take revision parameters
which denote other objects than commits, e.g. blobs ("files") or trees ("directories
of files").

[Continue...](/tutorials/revisions/overview)

### Reset, checkout and revert

<i>Source: [Atlassian](https://www.atlassian.com/git/tutorials/resetting-checking-out-and-reverting "|")</i>

The `git reset`, `git checkout`, and `git revert` commands are all similar in
that they undo some type of change in your repository. But, they all affect
different combinations of the working directory, staged snapshot, and commit
history. This article clearly defines how these commands differ and when each of
them should be used in the standard Git workflows.

[Continue...](/tutorials/reset-checkout-revert/overview)

### Merging vs. rebasing

<i>Source: [Atlassian](https://www.atlassian.com/git/tutorials/merging-vs-rebasing "|")</i>

The `git rebase` command has a reputation for being magical Git voodoo that beginners
should stay away from, but it can actually make life much easier for a development
team when used with care. In this article, we’ll compare `git rebase` with the related
`git merge` command and identify all of the potential opportunities to incorporate
rebasing into the typical Git workflow.

[Continue...](/tutorials/merge-rebase/overview)
