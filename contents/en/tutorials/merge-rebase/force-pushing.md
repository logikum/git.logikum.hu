<!-- ======================================================================
--- Search engine
title:          force-pushing
keywords:       tutorials, merge, rebase
description:    The force-pushing in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Force-pushing

If you try to push the rebased `master` branch back to a remote repository, Git will
prevent you from doing so because it conflicts with the remote `master` branch. But, you
can force the push to go through by passing the `--force` flag, like so:

```git
# Be very careful with this command!
git push --force
```

This overwrites the remote `master` branch to match the rebased one from your repository
and makes things very confusing for the rest of your team. So, be very careful to use
this command only when you know exactly what you're doing.

One of the only times you should be force-pushing is when you've performed a local cleanup
_after_ you've pushed a private `feature` branch to a remote repository (e.g., for backup
purposes). This is like saying, "Oops, I didn't really want to push that original version
of the feature branch. Take the current one instead." Again, it's important that nobody is
working off of the commits from the original version of the `feature` branch.

[Continue...](/tutorials/merge-rebase/workflow)
