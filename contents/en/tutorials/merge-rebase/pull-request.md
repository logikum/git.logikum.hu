<!-- ======================================================================
--- Search engine
title:          review with pull request
keywords:       tutorials, merge, rebase
description:    Reviewing a feature with a pull request in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Reviewing a feature with a pull request

f you use pull requests as part of your code review process, you need to avoid using
`git rebase` after creating the pull request. As soon as you make the pull request,
other developers will be looking at your commits, which means that it's a _public_ branch.
Re-writing its history will make it impossible for Git and your teammates to track any
follow-up commits added to the feature.

Any changes from other developers need to be incorporated with `git merge` instead of
`git rebase`.

For this reason, it's usually a good idea to clean up your code with an interactive
rebase _before_ submitting your pull request.

[Continue...](/tutorials/merge-rebase/approved-feature)
