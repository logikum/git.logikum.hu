<!-- ======================================================================
--- Search engine
title:          approved feature
keywords:       tutorials, merge, rebase
description:    Integrating an approved feature in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Integrating an approved feature

After a feature has been approved by your team, you have the option of rebasing the
feature onto the tip of the `master` branch before using `git merge` to integrate the
feature into the main code base.

This is a similar situation to incorporating upstream changes into a feature branch,
but since you're not allowed to re-write commits in the `master` branch, you have to
eventually use `git merge` to integrate the feature. However, by performing a rebase
before the merge, you're assured that the merge will be fast-forwarded, resulting in
a perfectly linear history. This also gives you the chance to squash any follow-up
commits added during a pull request.

![10](/images/tutorials/merge-rebase/10.svg)

If you're not entirely comfortable with `git rebase`, you can always perform the rebase
in a temporary branch. That way, if you accidentally mess up your feature's history, you
can check out the original branch and try again. For example:

```git
git checkout feature
git checkout -b temporary-branch
git rebase -i master
# [Clean up the history]
git checkout master
git merge temporary-branch
```

[Continue...](/tutorials/merge-rebase/summary)
