<!-- ======================================================================
--- Search engine
title:          local cleanup
keywords:       tutorials, merge, rebase
description:    Local cleanup using rebase in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Local cleanup

One of the best ways to incorporate rebasing into your workflow is to clean up local,
in-progress features. By periodically performing an interactive rebase, you can make
sure each commit in your feature is focused and meaningful. This lets you write your
code without worrying about breaking it up into isolated commits &dash; you can fix it
up after the fact.

When calling `git rebase`, you have two options for the new base: The feature's parent
branch (e.g., `master`), or an earlier commit in your feature. We saw an example of the
first option in the _Interactive rebasing_ section. The latter option is nice when you
only need to fix up the last few commits. For example, the following command begins an
interactive rebase of only the last 3 commits.

```git
git checkout feature
git rebase -i HEAD~3
```

By specifying `HEAD~3` as the new base, you're not actually moving the branch &dash;
you're just interactively re-writing the 3 commits that follow it. Note that this will
not incorporate upstream changes into the 'feature' branch.

![07](/images/tutorials/merge-rebase/07.svg)

If you want to re-write the entire feature using this method, the `git merge-base`
command can be useful to find the original base of the `feature` branch. The following
returns the commit ID of the original base, which you can then pass to `git rebase`:

```git
git merge-base feature master
```

This use of interactive rebasing is a great way to introduce `git rebase` into your
workflow, as it only affects local branches. The only thing other developers will see
is your finished product, which should be a clean, easy-to-follow feature branch history.

But again, this only works for _private_ feature branches. If you're collaborating with
other developers via the same feature branch, that branch is _public_, and you're not
allowed to re-write its history.

There is no `git merge` alternative for cleaning up local commits with an interactive
rebase.

[Continue...](/tutorials/merge-rebase/upstream-changes)
