<!-- ======================================================================
--- Search engine
title:          merge rebase
keywords:       tutorials, merge, rebase
description:    Comparing the merge and rebase commands of Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Merging vs. rebasing

### Conceptual overview

The first thing to understand about `git rebase` is that it solves the same problem
as `git merge`. Both of these commands are designed to integrate changes from one
branch into another branch &dash; they just do it in very different ways.

Consider what happens when you start working on a new feature in a dedicated branch,
then another team member updates the `master` branch with new commits. This results
in a forked history, which should be familiar to anyone who has used Git as a
collaboration tool.

![01](/images/tutorials/merge-rebase/01.svg)

Now, let's say that the new commits in `master` are relevant to the feature that
you're working on. To incorporate the new commits into your `feature` branch, you
have two options: merging or rebasing.

[Continue...](/tutorials/merge-rebase/merge)
