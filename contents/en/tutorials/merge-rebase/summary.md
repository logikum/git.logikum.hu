<!-- ======================================================================
--- Search engine
title:          command summary
keywords:       tutorials, merge, rebase
description:    Summary of the merge and rebase commands of Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Summary

And that's all you really need to know to start rebasing your branches. If you would
prefer a clean, linear history free of unnecessary merge commits, you should reach for
`git rebase` instead of `git merge` when integrating changes from another branch.

On the other hand, if you want to preserve the complete history of your project and
avoid the risk of re-writing public commits, you can stick with `git merge`. Either
option is perfectly valid, but at least now you have the option of leveraging the benefits
of `git rebase`.
