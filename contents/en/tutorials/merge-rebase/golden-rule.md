<!-- ======================================================================
--- Search engine
title:          golden rule
keywords:       tutorials, merge, rebase
description:    The golden rule of rebasing in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# The golden rule of rebasing

Once you understand what rebasing is, the most important thing to learn is when _not_
to do it. The golden rule of `git rebase` is to never use it on _public_ branches.

For example, think about what would happen if you rebased `master` onto your `feature`
branch:

![05](/images/tutorials/merge-rebase/05.svg)

The rebase moves all of the commits in `master` onto the tip of feature. The problem is
that this only happened in _your_ repository. All of the other developers are still working
with the original `master`. Since rebasing results in brand new commits, Git will think
that your `master` branch's history has diverged from everybody else's.

The only way to synchronize the two `master` branches is to merge them back together,
resulting in an extra merge commit _and_ two sets of commits that contain the same changes
(the original ones, and the ones from your rebased branch). Needless to say, this is a
very confusing situation.

So, before you run `git rebase`, always ask yourself, "Is anyone else looking at this branch?"
If the answer is yes, take your hands off the keyboard and start thinking about a
non-destructive way to make your changes (e.g., the `git revert` command). Otherwise, you're
safe to re-write history as much as you like.

[Continue...](/tutorials/merge-rebase/force-pushing)
