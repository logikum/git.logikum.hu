<!-- ======================================================================
--- Search engine
title:          upstream changes
keywords:       tutorials, merge, rebase
description:    Incorporating upstream changes into a feature in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Incorporating upstream changes into a feature

In the _Conceptual overview_ section, we saw how a `feature` branch can incorporate
upstream changes from `master` using either `git merge` or `git rebase`. Merging is
a safe option that preserves the entire history of your repository, while rebasing
creates a linear history by moving your feature branch onto the tip of `master`.

This use of `git rebase` is similar to a local cleanup (and can be performed simultaneously),
but in the process it incorporates those upstream commits from `master`.

Keep in mind that it's perfectly legal to rebase onto a remote branch instead of `master`.
This can happen when collaborating on the same feature with another developer and you need
to incorporate their changes into your repository.

For example, if you and another developer named John added commits to the `feature` branch,
your repository might look like the following after fetching the remote `feature` branch
from John�s repository:

![08](/images/tutorials/merge-rebase/08.svg)

You can resolve this fork the exact same way as you integrate upstream changes from
`master`: either merge your local `feature` with `john/feature`, or rebase your local
`feature` onto the tip of `john/feature`.

![09](/images/tutorials/merge-rebase/09.svg)

Note that this rebase doesn't violate [the golden rule of rebasing](golden-rule) because
only your local `feature` commits are being moved &dash; everything before that is untouched.
This is like saying, "add my changes to what John has already done." In most circumstances,
this is more intuitive than synchronizing with the remote branch via a merge commit.

By default, the `git pull` command performs a merge, but you can force it to integrate the
remote branch with a rebase by passing it the `--rebase` option.

[Continue...](/tutorials/merge-rebase/pull-request)
