<!-- ======================================================================
--- Search engine
title:          rebase option
keywords:       tutorials, merge, rebase
description:    The rebase option in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# The rebase option

As an alternative to merging, you can rebase the `feature` branch onto `master` branch
using the following commands:

```git
git checkout feature
git rebase master
```

This moves the entire `feature` branch to begin on the tip of the `master` branch,
effectively incorporating all of the new commits in `master`. But, instead of using a
merge commit, rebasing _re-writes_ the project history by creating brand new commits
for each commit in the original branch.

![03](/images/tutorials/merge-rebase/03.svg)

The major benefit of rebasing is that you get a much cleaner project history. First,
it eliminates the unnecessary merge commits required by `git merge`. Second, as you can
see in the above diagram, rebasing also results in a perfectly linear project history
&dash; you can follow the tip of feature all the way to the beginning of the project
without any forks. This makes it easier to navigate your project with commands like
`git log`, `git bisect`, and `gitk`.

But, there are two trade-offs for this pristine commit history: safety and traceability.
If you don't follow [the golden rule of rebasing](golden-rule), re-writing project history
can be potentially catastrophic for your collaboration workflow. And, less importantly,
rebasing loses the context provided by a merge commit &dash; you can't see when upstream
changes were incorporated into the feature.

[Continue...](/tutorials/merge-rebase/interactive)
