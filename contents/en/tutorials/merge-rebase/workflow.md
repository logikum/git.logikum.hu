<!-- ======================================================================
--- Search engine
title:          workflow
keywords:       tutorials, merge, rebase
description:    Workflow walkthrough using rebase in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Workflow walkthrough

Rebasing can be incorporated into your existing Git workflow as much or as little
as your team is comfortable with. In this section, we'll take a look at the benefits
that rebasing can offer at the various stages of a feature's development.

The first step in any workflow that leverages git rebase is to create a dedicated
branch for each feature. This gives you the necessary branch structure to safely
utilize rebasing:

![06](/images/tutorials/merge-rebase/06.svg)

[Continue...](/tutorials/merge-rebase/local-cleanup)
