<!-- ======================================================================
--- Search engine
title:          interactive rebasing
keywords:       tutorials, merge, rebase
description:    The interactive rebasing in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# Interactive rebasing

Interactive rebasing gives you the opportunity to alter commits as they are moved
to the new branch. This is even more powerful than an automated rebase, since it
offers complete control over the branch's commit history. Typically, this is used
to clean up a messy history before merging a `feature` branch into `master`.

To begin an interactive rebasing session, pass the `i` option to the `git rebase` command:

```git
git checkout feature
git rebase -i master
```

This will open a text editor listing all of the commits that are about to be moved:

```text
pick 33d5b7a Message for commit #1
pick 9480b3d Message for commit #2
pick 5c67e61 Message for commit #3
```

This listing defines exactly what the branch will look like after the rebase is
performed. By changing the `pick` command and/or re-ordering the entries, you can make
the branch's history look like whatever you want. For example, if the 2nd commit fixes
a small problem in the 1st commit, you can condense them into a single commit with
the `fixup` command:

```text
pick 33d5b7a Message for commit #1
fixup 9480b3d Message for commit #2
pick 5c67e61 Message for commit #3
```

When you save and close the file, Git will perform the rebase according to your
instructions, resulting in project history that looks like the following:

![02](/images/tutorials/merge-rebase/02.svg)

Eliminating insignificant commits like this makes your feature's history much easier
to understand. This is something that `git merge` simply cannot do.

[Continue...](/tutorials/merge-rebase/golden-rule)
