<!-- ======================================================================
--- Search engine
title:          merge option
keywords:       tutorials, merge, rebase
description:    The merge option in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-merge-rebase
searchable:     true
======================================================================= -->

# The merge option

The easiest option is to merge the `master` branch into the `feature` branch using
something like the following:

```git
git checkout feature
git merge master
```

Or, you can condense this to a one-liner:

```git
git merge master feature
```

This creates a new "merge commit" in the `feature` branch that ties together the
histories of both branches, giving you a branch structure that looks like this:

![02](/images/tutorials/merge-rebase/02.svg)

Merging is nice because it's a _non-destructive_ operation. The existing branches
are not changed in any way. This avoids all of the potential pitfalls of rebasing
(discussed below).

On the other hand, this also means that the `feature` branch will have an extraneous
merge commit every time you need to incorporate upstream changes. If `master` is
very active, this can pollute your `feature` branch's history quite a bit. While
it's possible to mitigate this issue with advanced `git log` options, it can make it
hard for other developers to understand the history of the project.

[Continue...](/tutorials/merge-rebase/rebase)
