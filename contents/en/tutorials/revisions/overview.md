<!-- ======================================================================
--- Search engine
title:          revisions
keywords:       tutorials, revisions
description:    The revisions in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-revisions
searchable:     true
======================================================================= -->

# Revisions

Many Git commands take revision parameters as arguments. Depending on the command,
they denote a specific commit or, for commands which walk the revision graph (such
as `git log`), all commits which are reachable from that commit. For commands that
walk the revision graph one can also specify a range of revisions explicitly.

In addition, some Git commands (such as `git show`) also take revision parameters
which denote other objects than commits, e.g. blobs ("files") or trees ("directories
of files").

### Specifying revisions

* [Extended SHA-1 revisions](/tutorials/revisions/extended-sha1)
* [@ construct revisions](/tutorials/revisions/at-constructs)
* [Blob or tree revisions](/tutorials/revisions/blob-tree)

### Specifying ranges

* [Revision range notations](/tutorials/revisions/range-notations)
* [Revision range summary](/tutorials/revisions/range-summary)

[Continue...](/tutorials/revisions/extended-sha1)
