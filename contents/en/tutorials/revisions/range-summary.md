<!-- ======================================================================
--- Search engine
title:          revision range summary
keywords:       tutorials, revisions, range summary
description:    The summary of revision range notations in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-revisions
searchable:     true
======================================================================= -->

# Revision range summary

### &lt;rev>

Include commits that are reachable from _&lt;rev>_ (i.e. _&lt;rev>_ and its ancestors).

### ^&lt;rev>
    
Exclude commits that are reachable from _&lt;rev>_ (i.e. _&lt;rev>_ and its ancestors).

### &lt;rev1>..&lt;rev2>
    
Include commits that are reachable from _&lt;rev2>_ but exclude those that are reachable
from _&lt;rev1>_. When either _&lt;rev1>_ or _&lt;rev2>_ is omitted, it defaults to `HEAD`.

### &lt;rev1>...&lt;rev2>
    
Include commits that are reachable from either _&lt;rev1>_ or _&lt;rev2>_ but exclude
those that are reachable from both. When either _&lt;rev1>_ or _&lt;rev2>_ is omitted,
it defaults to `HEAD`.

### &lt;rev>^@

```git
e.g. HEAD^@
```
    
A suffix ^ followed by an at sign is the same as listing all parents of _&lt;rev>_
(meaning, include anything reachable from its parents, but not the commit itself).

### &lt;rev>^!

```git
e.g. HEAD^!
```
    
A suffix ^ followed by an exclamation mark is the same as giving commit _&lt;rev>_
and then all its parents prefixed with ^ to exclude them (and their ancestors).

### &lt;rev>^-&lt;n>

```git
e.g. HEAD^-, HEAD^-2
```
    
Equivalent to _&lt;rev>^&lt;n>..&lt;rev>_, with &lt;n> = 1 if not given.

---

Here are a handful of examples using the Loeliger illustration above, with each
step in the notation’s expansion and selection carefully spelt out:

```git
Args   Expanded arguments    Selected commits
D                            G H D
D F                          G H I J D F
^G D                         H D
^D B                         E I J F B
^D B C                       E I J F B C
C                            I J F C
B..C   = ^B C                C
B...C  = B ^F C              G H D E B C
B^-    = B^..B
= ^B^1 B              E I J F B
C^@    = C^1
= F                   I J F
B^@    = B^1 B^2 B^3
= D E F               D G H E F I J
C^!    = C ^C^@
= C ^C^1
= C ^F                C
B^!    = B ^B^@
= B ^B^1 ^B^2 ^B^3
= B ^D ^E ^F          B
F^! D  = F ^I ^J D           G H D F
```