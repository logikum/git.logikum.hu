<!-- ======================================================================
--- Search engine
title:          extended SHA-1 revisions
keywords:       tutorials, revisions, extended sha-1
description:    The extended SHA1 revisions in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-revisions
searchable:     true
======================================================================= -->

# Extended SHA-1 revisions

### &lt;sha1>

```git
e.g. dae86e1950b1277e545cee180551750029cfe735, dae86e
```

The full SHA-1 object name (40-byte hexadecimal string), or a leading substring
that is unique within the repository. E.g. _dae86e1950b1277e545cee180551750029cfe735_
and _dae86e_ both name the same commit object if there is no other object in your
repository whose object name starts with dae86e.

### &lt;describeOutput>

```git
e.g. v1.7.4.2-679-g3bee7fb
```

Output from `git describe`; i.e. a closest tag, optionally followed by a dash and
a number of commits, followed by a dash, a g, and an abbreviated object name.

### &lt;refname>

```git
e.g. master, heads/master, refs/heads/master
```
    
A symbolic ref name. E.g. _master_ typically means the commit object referenced by
_refs/heads/master_. If you happen to have both _heads/master_ and _tags/master_,
you can explicitly say _heads/master_ to tell Git which one you mean. When ambiguous, 
a _&lt;refname>_ is disambiguated by taking the first match in the following rules:

1. If _$GIT_DIR/&lt;refname>_ exists, that is what you mean (this is usually useful
   only for `HEAD`, `FETCH_HEAD`, `ORIG_HEAD`, `MERGE_HEAD` and `CHERRY_PICK_HEAD`);
2. otherwise, _refs/&lt;refname>_ if it exists;
3. otherwise, _refs/tags/&lt;refname>_ if it exists;
4. otherwise, _refs/heads/&lt;refname>_ if it exists;
5. otherwise, _refs/remotes/&lt;refname>_ if it exists;
6. otherwise, _refs/remotes/&lt;refname>/HEAD_ if it exists.

   `HEAD` names the commit on which you based the changes in the working tree.
   `FETCH_HEAD` records the branch which you fetched from a remote repository
   with your last `git fetch` invocation. `ORIG_HEAD` is created by commands that
   move your `HEAD` in a drastic way, to record the position of the `HEAD` before
   their operation, so that you can easily change the tip of the branch back to
   the state before you ran them. `MERGE_HEAD` records the commit(s) which you are
   merging into your branch when you run `git merge`. `CHERRY_PICK_HEAD` records
   the commit which you are cherry-picking when you run `git cherry-pick`.

   Note that any of the _refs/*_ cases above may come either from the _$GIT_DIR/refs_
   directory or from the _$GIT_DIR/packed-refs_ file. While the ref name encoding
   is unspecified, UTF-8 is preferred as some output processing may assume ref
   names in UTF-8.

[Continue...](/tutorials/revisions/at-constructs)
