<!-- ======================================================================
--- Search engine
title:          revision range notations
keywords:       tutorials, revisions, range notations
description:    The revision range notations in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-revisions
searchable:     true
======================================================================= -->

# Revision range notations

History traversing commands such as `git log` operate on a set of commits, not
just a single commit.

For these commands, specifying a single revision, using the notation described in
the previous section, means the set of commits `reachable` from the given commit.

A commit’s reachable set is the commit itself and the commits in its ancestry chain.

### Commit Exclusions

**^&lt;rev> (caret) Notation**

To exclude commits reachable from a commit, a prefix ^ notation is used. E.g.
_^r1 r2_ means commits reachable from _r2_ but exclude the ones reachable from
_r1_ (i.e. _r1_ and its ancestors).

### Dotted Range Notations

**The .. (two-dot) Range Notation**

The _^r1 r2_ set operation appears so often that there is a shorthand for it.
When you have two commits _r1_ and _r2_ (named according to the syntax explained
in SPECIFYING REVISIONS above), you can ask for commits that are reachable from
_r2_ excluding those that are reachable from _r1_ by _^r1 r2_ and it can be written
as _r1..r2_.

**The …​ (three dot) Symmetric Difference Notation**

A similar notation _r1...r2_ is called symmetric difference of _r1_ and _r2_ and
is defined as _r1 r2 --not $(git merge-base --all r1 r2)_. It is the set of commits
that are reachable from either one of _r1_ (left side) or _r2_ (right side) but
not from both.

---

In these two shorthand notations, you can omit one end and let it default to `HEAD`.
For example, _origin.._ is a shorthand for _origin..HEAD_ and asks "What did I do
since I forked from the origin branch?" Similarly, _..origin_ is a shorthand for
_HEAD..origin_ and asks "What did the origin do since I forked from them?" Note that
_.._ would mean _HEAD..HEAD_ which is an empty range that is both reachable and
unreachable from `HEAD`.

### Other &lt;rev>^ Parent Shorthand Notations

Three other shorthands exist, particularly useful for merge commits, for naming a
set that is formed by a commit and its parent commits.

The _r1^@_ notation means all parents of _r1_.

The _r1^!_ notation includes commit _r1_ but excludes all of its parents. By itself,
this notation denotes the single commit _r1_.

The _&lt;rev>^-&lt;n>_ notation includes _&lt;rev>_ but excludes the &lt;n>th parent (i.e. a
shorthand for _&lt;rev>^&lt;n>..&lt;rev>_), with &lt;n> = 1 if not given. This is typically
useful for merge commits where you can just pass _&lt;commit>^-_ to get all the commits
in the branch that was merged in merge commit _&lt;commit>_ (including _&lt;commit>_ itself).

While _&lt;rev>^&lt;n>_ was about specifying a single commit parent, these three notations
also consider its parents. For example you can say _HEAD^2^@_, however you cannot
say _HEAD^@^2_.

[Continue...](/tutorials/revisions/range-summary)
