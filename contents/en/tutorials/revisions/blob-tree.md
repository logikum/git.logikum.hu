<!-- ======================================================================
--- Search engine
title:          blob or tree revisions
keywords:       tutorials, revisions, blob, tree
description:    The blob or tree revisions in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-revisions
searchable:     true
======================================================================= -->

# Blob or tree revisions

### &lt;rev>^

```git
e.g. HEAD^, v1.5.1^0
```
    
A suffix ^ to a revision parameter means the first parent of that commit object.
_^&lt;n>_ means the &lt;n>th parent (i.e. _&lt;rev>^_ is equivalent to _&lt;rev>^1_). As a
special rule, _&lt;rev>^0_ means the commit itself and is used when _&lt;rev>_ is the
object name of a tag object that refers to a commit object.

### &lt;rev>~&lt;n>

```git
e.g. master~3
```
    
A suffix _~&lt;n>_ to a revision parameter means the commit object that is the &lt;n>th
generation ancestor of the named commit object, following only the first parents.
I.e. _&lt;rev>~3_ is equivalent to _&lt;rev>^^^_ which is equivalent to _&lt;rev>^1^1^1_.
See below for an illustration of the usage of this form.

### &lt;rev>^{&lt;type>}

```git
e.g. v0.99.8^{commit}
```
    
A suffix ^ followed by an object type name enclosed in brace pair means dereference
the object at _&lt;rev>_ recursively until an object of type _&lt;type>_ is found or the
object cannot be dereferenced anymore (in which case, barf). For example, if _&lt;rev>_
is a commit-ish, _&lt;rev>^{commit}_ describes the corresponding commit object.
Similarly, if _&lt;rev>_ is a tree-ish, _&lt;rev>^{tree}_ describes the corresponding
tree object. _&lt;rev>^0_ is a short-hand for _&lt;rev>^{commit}_.

_rev^{object}_ can be used to make sure _rev_ names an object that exists, without
requiring _rev_ to be a tag, and without dereferencing _rev_; because a tag is already
an object, it does not have to be dereferenced even once to get to an object.

_rev^{tag}_ can be used to ensure that rev identifies an existing tag object.

### &lt;rev>^{}

```git
e.g. v0.99.8^{}
```
    
A suffix ^ followed by an empty brace pair means the object could be a tag, and
dereference the tag recursively until a non-tag object is found.

### &lt;rev>^{/&lt;text>}

```git
e.g. HEAD^{/fix nasty bug}
```
    
A suffix ^ to a revision parameter, followed by a brace pair that contains a text
led by a slash, is the same as the _:/fix nasty bug_ syntax below except that it
returns the youngest matching commit which is reachable from the _&lt;rev>_ before ^.

### /&lt;text>

```git
e.g. :/fix nasty bug
```
    
A colon, followed by a slash, followed by a text, names a commit whose commit message
matches the specified regular expression. This name returns the youngest matching
commit which is reachable from any ref. The regular expression can match any part
of the commit message. To match messages starting with a string, one can use e.g.
_:/^foo_. The special sequence _:/!_ is reserved for modifiers to what is matched.
_:/!-foo_ performs a negative match, while _:/!!foo_ matches a literal ! character,
followed by foo. Any other sequence beginning with _:/!_ is reserved for now.

### &lt;rev>:&lt;path>

```git
e.g. HEAD:README, :README, master:./README
```
    
A suffix : followed by a path names the blob or tree at the given path in the tree-ish
object named by the part before the colon. _:path_ (with an empty part before the colon)
is a special case of the syntax described next: content recorded in the index at the
given path. A path starting with ./ or ../ is relative to the current working directory.
The given path will be converted to be relative to the working tree’s root directory.
This is most useful to address a blob or tree from a commit or tree that has the same
tree structure as the working tree.

### :&lt;n>:&lt;path>

```git
e.g. :0:README, :README
```
    
A colon, optionally followed by a stage number (0 to 3) and a colon, followed by
a path, names a blob object in the index at the given path. A missing stage number
(and the colon that follows it) names a stage 0 entry. During a merge, stage 1 is
the common ancestor, stage 2 is the target branch’s version (typically the current
branch), and stage 3 is the version from the branch which is being merged.

---

Here is an illustration, by Jon Loeliger. Both commit nodes B and C are parents
of commit node A. Parent commits are ordered left-to-right.

```git
G   H   I   J
 \ /     \ /
  D   E   F
   \  |  / \
    \ | /   |
     \|/    |
      B     C
       \   /
        \ /
         A

A =      = A^0
B = A^   = A^1     = A~1
C = A^2  = A^2
D = A^^  = A^1^1   = A~2
E = B^2  = A^^2
F = B^3  = A^^3
G = A^^^ = A^1^1^1 = A~3
H = D^2  = B^^2    = A^^^2  = A~2^2
I = F^   = B^3^    = A^^3^
J = F^2  = B^3^2   = A^^3^2
```

[Continue...](/tutorials/revisions/range-notations)
