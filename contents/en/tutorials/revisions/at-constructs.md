<!-- ======================================================================
--- Search engine
title:          @ construct revisions
keywords:       tutorials, revisions, @ constructs
description:    The @ construct revisions in Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-revisions
searchable:     true
======================================================================= -->

# @ construct revisions

### @

@ alone is a shortcut for `HEAD`.

### &lt;refname>@{&lt;date>}

```git
e.g. master@{yesterday}, HEAD@{5 minutes ago}
```
    
A ref followed by the suffix @ with a date specification enclosed in a brace pair
(e.g. _{yesterday}_, _{1 month 2 weeks 3 days 1 hour 1 second ago}_ or _{1979-02-26 18:30:00}_)
specifies the value of the ref at a prior point in time. This suffix may only be 
used immediately following a ref name and the ref must have an existing log
(_$GIT_DIR/logs/&lt;ref>_). Note that this looks up the state of your **local/** ref
at a given time; e.g., what was in your local master branch last week. If you want
to look at commits made during certain times, see `--since` and `--until`.

### &lt;refname>@{&lt;n>}

```git
e.g. master@{1}
```
    
A ref followed by the suffix @ with an ordinal specification enclosed in a brace
pair (e.g. _{1}_, _{15}_) specifies the n-th prior value of that ref. For example
_master@{1}_ is the immediate prior value of _master_ while _master@{5}_ is the
5th prior value of _master_. This suffix may only be used immediately following
a ref name and the ref must have an existing log (_$GIT_DIR/logs/&lt;refname>_).

### @{&lt;n>}

```git
e.g. @{1}
```
    
You can use the @ construct with an empty ref part to get at a reflog entry of the
current branch. For example, if you are on branch _blabla_ then _@{1}_ means the
same as _blabla@{1}_.

### @{-&lt;n>}

```git
e.g. @{-1}
```
    
The construct _@{-&lt;n>}_ means the &lt;n>th branch/commit checked out before the current one.

### &lt;branchname>@{upstream}

```git
e.g. master@{upstream}, @{u}
```
    
The suffix _@{upstream}_ to a branchname (short form _&lt;branchname>@{u}_) refers
to the branch that the branch specified by _branchname_ is set to build on top of
(configured with `branch.&lt;name>.remote` and `branch.&lt;name>.merge`). A missing
_branchname_ defaults to the current one. These suffixes are also accepted when
spelled in uppercase, and they mean the same thing no matter the case.

### &lt;branchname>@{push}

```git
e.g. master@{push}, @{push}
```
    
The suffix _@{push}_ reports the branch "where we would push to" if `git push`
were run while _branchname_ was checked out (or the current `HEAD` if no _branchname_
is specified). Since our push destination is in a remote repository, of course,
we report the local tracking branch that corresponds to that branch (i.e., something
in _refs/remotes/_).

Here’s an example to make it more clear:

```git
$ git config push.default current
$ git config remote.pushdefault myfork
$ git checkout -b mybranch origin/master

$ git rev-parse --symbolic-full-name @{upstream}
refs/remotes/origin/master

$ git rev-parse --symbolic-full-name @{push}
refs/remotes/myfork/mybranch
```

Note in the example that we set up a triangular workflow, where we pull from one
location and push to another. In a non-triangular workflow, _@{push}_ is the same
as _@{upstream}_, and there is no need for it.

This suffix is also accepted when spelled in uppercase, and means the same thing
no matter the case.

[Continue...](/tutorials/revisions/blob-tree)
