<!-- ======================================================================
--- Search engine
title:          file-level reset
keywords:       tutorials, file-level, reset
description:    The file-level reset command of Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-reset-checkout-revert
searchable:     true
======================================================================= -->

# File-level reset

When invoked with a file path, `git reset` updates the _staged snapshot_ to match
the version from the specified commit. For example, this command will fetch the
version of `foo.py` in the 2nd-to-last commit and stage it for the next commit:

```git
git reset HEAD~2 foo.py
```

As with the commit-level version of `git reset`, this is more commonly used with
`HEAD` rather than an arbitrary commit. Running `git reset HEAD foo.py` will unstage
`foo.py`. The changes it contains will still be present in the working directory.

![07](/images/tutorials/reset-checkout-revert/07.svg)

The `--soft`, `--mixed`, and `--hard` flags do not have any effect on the file-level
version of `git reset`, as the staged snapshot is _always_ updated, and the working
directory is _never_ updated.

[Continue...](/tutorials/reset-checkout-revert/fl-checkout)
