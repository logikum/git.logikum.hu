<!-- ======================================================================
--- Search engine
title:          commit-level revert
keywords:       tutorials, commit-level, revert
description:    The commit-level revert command of Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-reset-checkout-revert
searchable:     true
======================================================================= -->

# Commit-level revert

Reverting undoes a commit by creating a new commit. This is a safe way to undo
changes, as it has no chance of re-writing the commit history. For example, the
following command will figure out the changes contained in the 2nd to last commit,
create a new commit undoing those changes, and tack the new commit onto the
existing project.

```git
git checkout hotfix
git revert HEAD~2
```

This can be visualized as the following:

![06](/images/tutorials/reset-checkout-revert/06.svg)

Contrast this with `git reset`, which does alter the existing commit history.
For this reason, `git revert` should be used to undo changes on a public branch,
and `git reset` should be reserved for undoing changes on a private branch.

You can also think of `git revert` as a tool for undoing _committed_ changes,
while `git reset HEAD` is for undoing _uncommitted_ changes.

Like `git checkout`, `git revert` has the potential to overwrite files in the
working directory, so it will ask you to commit or stash changes that would be
lost during the revert operation.

[Continue...](/tutorials/reset-checkout-revert/fl-reset)
