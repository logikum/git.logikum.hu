<!-- ======================================================================
--- Search engine
title:          command summary
keywords:       tutorials, summary, reset, checkout, revert
description:    The overview of reset, checkout, and revert commands of Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-reset-checkout-revert
searchable:     true
======================================================================= -->

# Summary

You should now have all the tools you could ever need to undo changes in a Git
repository. The `git reset`, `git checkout`, and `git revert` commands can be
confusing, but when you think about their effects on the working directory, staged
snapshot, and commit history, it should be easier to discern which command fits
the development task at hand.

The table below sums up the most common use cases for all of these commands. Be
sure to keep this reference handy, as you’ll undoubtedly need to use at least
some them during your Git career.

| %std% Command | Scope | Common use cases |
| ------------- | ----- | ---------------- |
| git reset     | commit-level | Discard commits in a private branch or throw away uncommited changes. |
| git reset     | file-level | Unstage a file. |
| git checkout  | commit-level | Switch between branches or inspect old snapshots. |
| git checkout  | file-level | Discard changes in the working directory. |
| git revert    | commit-level | Undo commits in a public branch. |
| git revert    | file-level | (N/A) |