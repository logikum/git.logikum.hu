<!-- ======================================================================
--- Search engine
title:          reset checkout revert
keywords:       tutorials, reset, checkout, revert
description:    The reset, checkout, and revert commands of Git.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-tut-reset-checkout-revert
searchable:     true
======================================================================= -->

# Reset, checkout and revert

The `git reset`, `git checkout`, and `git revert` command are some of the most
useful tools in your Git toolbox. They all let you undo some kind of change in
your repository, and the first two commands can be used to manipulate either
commits or individual files.

Because they’re so similar, it’s very easy to mix up which command should be
used in any given development scenario. In this article, we’ll compare the most
common configurations of `git reset`, `git checkout`, and `git revert`. Hopefully,
you’ll walk away with the confidence to navigate your repository using any of
these commands.

![01](/images/tutorials/reset-checkout-revert/01.svg)

It helps to think about each command in terms of their effect on the three main
components of a Git repository: the working directory, the staged snapshot, and
the commit history. Keep these components in mind as you read through this article.

### Commit-level operations

The parameters that you pass to `git reset` and `git checkout` determine their
scope. When you _don’t_ include a file path as a parameter, they operate on whole
commits. That’s what we’ll be exploring in this section. Note that `git revert`
has no file-level counterpart.

* [reset](/tutorials/reset-checkout-revert/cl-reset)
* [checkout](/tutorials/reset-checkout-revert/cl-checkout)
* [revert](/tutorials/reset-checkout-revert/cl-revert)

### File-level operations

The `git reset` and `git checkout` commands also accept an optional file path as
a parameter. This dramatically alters their behavior. Instead of operating on
entire snapshots, this forces them to limit their operations to a single file.

* [reset](/tutorials/reset-checkout-revert/fl-reset)
* [checkout](/tutorials/reset-checkout-revert/fl-checkout)

[Continue...](/tutorials/reset-checkout-revert/cl-reset)
