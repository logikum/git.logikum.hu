<!-- ======================================================================
--- Search engine
title:          git flow support
keywords:       AVH edition, support, graph
description:    The support graph of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       document-paper
layout:         layout-2-left
$-left:         left-graph
searchable:     false
======================================================================= -->

# git flow support

The next figure illustrates hte following GitFlow subcommands:

* git flow support start &lt;version.x> &lt;base>
* git flow hotfix start &lt;version.h> support/&lt;version.x>
* git flow hotfix finish &lt;version.h>
* git flow feature start &lt;name> support/&lt;version.x>
* git flow feature finish &lt;name>
* git flow release start &lt;version.r> support/&lt;version.x>
* git flow release finish &lt;version.r>

<canvas id="figure" resize="true" width="680" height="600"></canvas>

<script type="text/paperscript" src="/scripts/graphs/support.js" canvas="figure"></script>
