<!-- ======================================================================
--- Search engine
title:          git flow release
keywords:       AVH edition, release, graph
description:    The release graph of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       document-paper
layout:         layout-2-left
$-left:         left-graph
searchable:     false
======================================================================= -->

# git flow release

The next figure illustrates hte following GitFlow subcommands:

* git flow release start &lt;version> [&lt;base>]
* git flow release publish [&lt;version>]
* git flow release track &lt;version>
* git push origin release/&lt;version>
* git pull origin release/&lt;version>
* git flow release finish &lt;version>

<canvas id="figure" resize="true" width="680" height="600"></canvas>

<script type="text/paperscript" src="/scripts/graphs/release.js" canvas="figure"></script>
