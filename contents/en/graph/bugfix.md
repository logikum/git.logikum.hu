<!-- ======================================================================
--- Search engine
title:          git flow bugfix
keywords:       AVH edition, bugfix, graph
description:    The bugfix graph of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       document-paper
layout:         layout-2-left
$-left:         left-graph
searchable:     false
======================================================================= -->

# git flow bugfix

The next figure illustrates hte following GitFlow subcommands:

* git flow bugfix start &lt;name> [&lt;base>]
* git flow bugfix publish [&lt;name>]
* git flow bugfix track &lt;name>
* git push origin bugfix/&lt;name>
* git pull origin bugfix/&lt;name>
* git flow bugfix finish &lt;name>

<canvas id="figure" resize="true" width="680" height="600"></canvas>

<script type="text/paperscript" src="/scripts/graphs/bugfix.js" canvas="figure"></script>
