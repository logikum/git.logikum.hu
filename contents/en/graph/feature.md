<!-- ======================================================================
--- Search engine
title:          git flow feature
keywords:       AVH edition, feature, graph
description:    The feature graph of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       document-paper
layout:         layout-2-left
$-left:         left-graph
searchable:     false
======================================================================= -->

# git flow feature

The next figure illustrates hte following GitFlow subcommands:

* git flow feature start &lt;name> [&lt;base>]
* git flow feature publish [&lt;name>]
* git flow feature track &lt;name>
* git push origin feature/&lt;name>
* git pull origin feature/&lt;name>
* git flow feature finish &lt;name>

<canvas id="figure" resize="true" width="680" height="600"></canvas>

<script type="text/paperscript" src="/scripts/graphs/feature.js" canvas="figure"></script>
