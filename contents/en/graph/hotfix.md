<!-- ======================================================================
--- Search engine
title:          git flow hotfix
keywords:       AVH edition, hotfix, graph
description:    The hotfix graph of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       document-paper
layout:         layout-2-left
$-left:         left-graph
searchable:     false
======================================================================= -->

# git flow hotfix

The next figure illustrates hte following GitFlow subcommands:

* git flow hotfix start &lt;version> [&lt;base>]
* git flow hotfix publish [&lt;version>]
* git flow hotfix track &lt;version>
* git push origin hotfix/&lt;version>
* git pull origin hotfix/&lt;version>
* git flow hotfix finish &lt;version>

<canvas id="figure" resize="true" width="680" height="600"></canvas>

<script type="text/paperscript" src="/scripts/graphs/hotfix.js" canvas="figure"></script>
