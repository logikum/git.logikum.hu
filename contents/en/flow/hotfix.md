<!-- ======================================================================
--- Search engine
title:          git flow hotfix
keywords:       AVH edition, hotfix, subcommand
description:    The hotfix subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow hotfix

### help

Manage your hotfix branches.

```text
git flow hotfix [list]
git flow hotfix start
git flow hotfix finish
git flow hotfix publish
git flow hotfix track
git flow hotfix rebase
git flow hotfix delete
```

### list

Lists all local hotfix branches.

```text
git flow hotfix [list] [-h] [-v]
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| -v, --verbose | Verbose (more) output. |

### start

Start new hotfix branch named `version`, optionally base it on `base` instead of the `master` branch.

```text
git flow hotfix start [-h] [-F] <version> [<base>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |
| -F, --[no]fetch | Fetch from origin before performing local operation. |

### finish

Finish a hotfix branch `version`.

```text
git flow hotfix finish [-h] [-F] [-s] [-u] [-m | -f] [-p] [-k] [-n] [-b] [-S] <version>
```

|||
|------------------|-----------------------------------------------------------|
| -h, --help       | Show this help. |
| --showcommands   | Show git commands while executing them. |
| -F, --[no]fetch  | Fetch from origin before performing finish. |
| -s, --[no]sign   | Sign the hotfix tag cryptographically. |
| -u, --[no]signingkey | Use the given GPG-key for the digital signature (implies -s). |
| -m, --[no]message | Use the given tag message. |
| -f, --[no]messagefile ... | Use the contents of the given file as tag message. |
| -p, --[no]push   | Push to origin after performing finish. |
| -k, --[no]keep   | Keep branch after performing finish. |
| --[no]keepremote | Keep the remote branch. |
| --[no]keeplocal  | Keep the local branch. |
| -D, --[no]force_delete | Force delete hotfix branch after finish. |
| -n, --[no]notag  | Don't tag this hotfix. |
| -b, --[no]nobackmerge | Don't back-merge master, or tag if applicable, in develop. |
| -S, --[no]squash | Squash hotfix during merge. |
| -T, --tagname    | Use given tag name. |

### publish

Start sharing hotfix `version` on origin.

```text
git flow hotfix publish [-h] <version>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### track

Start tracking hotfix `version` that is shared on origin.

```text
git flow hotfix track [-h] <version>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### rebase

Rebase `name` on `base_branch`.

```text
git flow hotfix rebase [-h] [-i] [-p] [<name|nameprefix>]
```

|||
|---------------------------|--------------------------------------------------|
| -h, --help                | Show this help. |
| --showcommands            | Show git commands while executing them. |
| -i, --[no]interactive     | Do an interactive rebase. |
| -p, --[no]preserve-merges | Preserve merges. |

### delete

Delete the given hotfix branch.

```text
git flow hotfix delete [-h] [-f] [-r] <version>
```

|||
|------------------|-----------------------------------------------------------|
| -h, --help       | Show this help. |
| --showcommands   | Show git commands while executing them. |
| -f, --[no]force  | Force deletion. |
| -r, --[no]remote | Delete remote branch. |

<div>&nbsp;</div>
