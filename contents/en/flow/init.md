<!-- ======================================================================
--- Search engine
title:          git flow init
keywords:       AVH edition, init, subcommand
description:    The init subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow init

### help

Setup a git repository for GitFlow usage. Can also be used to start a git repository.

```text
git flow init [-h] [-d] [-f]
```

|||
|--------------------|---------------------------------------------------------|
| -h, --help         | Show this help. |
| --showcommands     | Show git commands while executing them. |
| -d, --[no]defaults | Use default branch naming conventions. |
| -f, --[no]force    | Force setting of gitflow branches, even if already configured. |
| --Use              | Config file location. |
| --local            | Use repository config file. |
| --global           | Use global config file. |
| --system           | Use system config file. |
| --file ...         | Use given config file. |

<div>&nbsp;</div>
