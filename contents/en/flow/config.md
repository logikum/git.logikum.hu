<!-- ======================================================================
--- Search engine
title:          git flow config
keywords:       AVH edition, config, subcommand
description:    The config subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow config

### help

Manage the GitFlow configuration.

```text
git flow config [list]
git flow config set
git flow config base
```

### list

Show the GitFlow configurations.

```text
git flow config [list]
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| --Use         / Config file location. |
| --local       | Use repository config file. |
| --global      | Use global config file. |
| --system      | Use system config file. |
| --file ...    | Use given config file. |

### set

Set the GitFlow configuration option to the given value.

```text
git flow config set <option> <value>
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| --local       | Use repository config file. |
| --global      | Use global config file. |
| --system      | Use system config file. |
| --file ...    | Use given config file. |

### base

Set the given `base` for the given `branch`.

```text
git flow config base [-h] <branch> [<base>]
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| --get         | Get the base for the given branch (default behavior). |
| --set         | Set the given base for the given branch. |

<div>&nbsp;</div>
