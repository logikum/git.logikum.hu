<!-- ======================================================================
--- Search engine
title:          git flow version
keywords:       AVH edition, version, subcommand
description:    The version subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow version

### help

Shows version information.

```text
git flow version
```
