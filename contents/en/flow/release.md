<!-- ======================================================================
--- Search engine
title:          git flow release
keywords:       AVH edition, release, subcommand
description:    The release subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow release

### help

Manage your release branches.

```text
git flow release [list]
git flow release start
git flow release finish
git flow release branch
git flow release publish
git flow release track
git flow release rebase
git flow release delete
```

### list

List existing release branches.

```text
git flow release [list] [-h] [-v]
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| -v, --verbose | Verbose (more) output. |

### start

Start new release branch.

```text
git flow release start [-h] [-F] [-v] <version> [<base>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |
| -F, --[no]fetch | Fetch from origin before performing local operation. |
| -v, --verbose   | Verbose (more) output. |

### finish

Finish a release branch.

```text
git flow release finish [-h] [-F] [-s] [-u] [-m | -f] [-p] [-k] [-n] [-b] [-S] <version>
```

|||
|------------------|-----------------------------------------------------------|
| -h, --help       | Show this help. |
| --showcommands   | Show git commands while executing them. |
| -F, --[no]fetch  | Fetch from origin before performing finish. |
| -s, --sign       | Sign the release tag cryptographically. |
| -u, --signingkey | Use the given GPG-key for the digital signature (implies -s). |
| -m, --message    | Use the given tag message. |
| -f, --[no]messagefile ... | Use the contents of the given file as a tag message. |
| -p, --[no]push   | Push to origin after performing finish. |
| --[no]pushproduction | Push the production branch. |
| --[no]pushdevelop | Push the develop branch. |
| --[no]pushtag    | Push the tag. |
| -k, --[no]keep   | Keep branch after performing finish. |
| --keepremote     | Keep the remote branch. |
| --keeplocal      | Keep the local branch. |
| -D, --[no]force_delete | Force delete release branch after finish. |
| -n, --[no]tag    | Don't tag this release. |
| -b, --[no]nobackmerge | Don't back-merge master, or tag if applicable, in develop. |
| -S, --[no]squash | Squash release during merge. |
| --[no]ff-master  | Fast forward master branch if possible. |
| -T, --tagname    | Use given tag name. |

### branch

Release a branch `name`, if a name is not given it defaults to the develop branch,
and use the given version `version`.

```text
git flow release branch [-h] [-F] [-s] [-u] [-m] [-f] [-p] [-n] [-S] <version> [<name>]
```

|||
|------------------|-----------------------------------------------------------|
| -h, --help       | Show this help. |
| -v, --verbose    | Verbose (more) output. |
| -F, --[no]fetch  | Fetch from origin before performing finish. |
| -s, --sign       | Sign the release tag cryptographically. |
| -u, --signingkey | Use the given GPG-key for the digital signature (implies -s). |
| -m, --message    | Use the given tag message. |
| -f, --[no]messagefile ... | Use the contents of the given file as a tag message. |
| -p, --[no]push   | Push to origin after performing finish. |
| -n, --[no]tag    | Don't tag this release. |
| -S, --[no]squash | Squash release during merge. |

### publish

Start sharing release `version` on origin.

```text
git flow release publish [-h] <version>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### track

Start tracking release `version` that is shared on origin.

```text
git flow release track [-h] <version>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### rebase

Rebase `name` on `base_branch`.

```text
git flow release rebase [-h] [-i] [-p] [<name|nameprefix>]
```

|||
|---------------------------|--------------------------------------------------|
| -h, --help                | Show this help. |
| --showcommands            | Show git commands while executing them. |
| -i, --[no]interactive     | Do an interactive rebase. |
| -p, --[no]preserve-merges | Preserve merges. |

### delete

Delete the given release branch.

```text
git flow release delete [-h] [-f] [-r] <name>
```

|||
|------------------|-----------------------------------------------------------|
| -h, --help       | Show this help. |
| --showcommands   | Show git commands while executing them. |
| -f, --[no]force  | Force deletion. |
| -r, --[no]remote | Delete remote branch. |

<div>&nbsp;</div>
