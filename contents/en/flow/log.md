<!-- ======================================================================
--- Search engine
title:          git flow log
keywords:       AVH edition, log, subcommand
description:    The log subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow log

### help

Shows current branch log compared to develop.

```text
git flow log
```

> See `git help log` for arguments.