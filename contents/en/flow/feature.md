<!-- ======================================================================
--- Search engine
title:          git flow feature
keywords:       AVH edition, feature, subcommand
description:    The feature subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow feature

### help

Manage your feature branches.

```text
git flow feature [list]
git flow feature start
git flow feature finish
git flow feature publish
git flow feature track
git flow feature diff
git flow feature checkout
git flow feature rebase
git flow feature pull
git flow feature delete
git flow feature rename
```

### list

Lists all the existing feature branches in the local repository.

```text
git flow feature [list] [-h] [-v]
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| -v, --verbose | Verbose (more) output. |

### start

Start new feature `name`, optionally basing it on `base` instead of `develop`.

```text
git flow feature start [-h] [-F] <name> [<base>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |
| -F, --[no]fetch | Fetch from origin before performing local operation. |

### finish

Finish feature `name`.

```text
git flow feature finish [-h] [-F] [-r] [-p] [-k] [-D] [-S] [--no-ff] <name|nameprefix>
```

|||
|------------------|-----------------------------------------------------------|
| -h, --help       | Show this help. |
| --showcommands   | Show git commands while executing them. |
| -F, --[no]fetch  | Fetch from origin before performing finish. |
| -r, --[no]rebase | Rebase before merging. |
| -p, --[no]preserve-merges | Preserve merges while rebasing. |
| --[no]push       | Push to origin after performing finish. |
| -k, --[no]keep   | Keep branch after performing finish. |
| --keepremote     | Keep the remote branch. |
| --keeplocal      | Keep the local branch. |
| -D, --[no]force_delete | Force delete feature branch after finish. |
| -S, --[no]squash | Squash feature during merge. |
| --no-ff          | Never fast-forward during the merge. |

### publish

Publish feature branch `name` on origin.
When `name` is omitted the current branch is used, but only if it's a feature branch.

```text
git flow feature publish [-h] [<name>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### track

Start tracking feature `name` that is shared on origin.

```text
git flow feature track [-h] <name>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### diff

Show all changes in `name` that are not in the base.

```text
git flow feature diff [-h] [<name|nameprefix>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### checkout

Switch to feature branch `name`.

```text
git flow feature checkout [-h] [<name|nameprefix>]
git flow feature co [-h] [<name|nameprefix>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### rebase

Rebase `name` on `base_branch`.

```text
git flow feature rebase [-h] [-i] [-p] [<name|nameprefix>]
```

|||
|---------------------------|--------------------------------------------------|
| -h, --help                | Show this help. |
| --showcommands            | Show git commands while executing them. |
| -i, --[no]interactive     | Do an interactive rebase. |
| -p, --[no]preserve-merges | Preserve merges. |

### pull

Pull feature `name` from `remote`.

> The command `git flow feature pull` will be deprecated per version 2.0.0.  
  Use `git flow feature track` instead.

```text
git flow feature pull [-h] <remote> [<name>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### delete

Delete a given feature branch.

```text
git flow feature delete [-h] [-f] [-r] <name>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |
| -f, --[no]force | Force deletion. |
| -r, --[no]remote | Delete remote branch. |

### rename

Rename a given feature branch.

```text
git flow feature rename [-h] <new_name> [<new_name>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

<div>&nbsp;</div>
