<!-- ======================================================================
--- Search engine
title:          git flow support
keywords:       AVH edition, support, subcommand
description:    The support subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow support

### help

Manage your support branches.

```text
git flow support [list]
git flow support start
git flow support rebase
```

### list

Lists all local support branches.

```text
git flow support [list] [-h] [-v]
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| -v, --verbose | Verbose (more) output. |

### start

Start a new support branch name `version` based on `base`.

```text
git flow support start [-h] [-F] [-v] <version> <base>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |
| -F, --[no]fetch | Fetch from origin before performing local operation. |

### rebase

Rebase `name` on `base_branch`.

```text
git flow support rebase [-h] [-i] [-p] [<name|nameprefix>]
```

|||
|---------------------------|--------------------------------------------------|
| -h, --help                | Show this help. |
| --showcommands            | Show git commands while executing them. |
| -i, --[no]interactive     | Do an interactive rebase. |
| -p, --[no]preserve-merges | Preserve merges. |

<div>&nbsp;</div>
