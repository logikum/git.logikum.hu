<!-- ======================================================================
--- Search engine
title:          git flow bugfix
keywords:       AVH edition, bugfix, subcommand
description:    The bugfix subcommand of GitFlow AVH Edition.
--- Menu system
order:          
text:           
hidden:         false
umbel:          false
--- Page properties
id:             
document:       
layout:         layout-2-left
$-left:         left-flow
searchable:     true
======================================================================= -->

# git flow bugfix

### help

Manage your bugfix branches.

```text
git flow bugfix [list]
git flow bugfix start
git flow bugfix finish
git flow bugfix publish
git flow bugfix track
git flow bugfix diff
git flow bugfix checkout
git flow bugfix rebase
git flow bugfix pull
git flow bugfix delete
git flow bugfix rename
```

### list

Lists all the existing bugfix branches in the local repository.

```text
git flow bugfix [list] [-h] [-v]
```

|||
|---------------|--------------------------------------------------------------|
| -h, --help    | Show this help. |
| -v, --verbose | Verbose (more) output. |

### start

Start new bugfix `name`, optionally basing it on `base` instead of `develop`.

```text
git flow bugfix start [-h] [-F] <name> [<base>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |
| -F, --[no]fetch | Fetch from origin before performing local operation. |

### finish

Finish bugfix `name`.

```text
git flow bugfix finish [-h] [-F] [-r] [-p] [-k] [-D] [-S] [--no-ff] <name|nameprefix>
```

|||
|------------------|-----------------------------------------------------------|
| -h, --help       | Show this help. |
| --showcommands   | Show git commands while executing them. |
| -F, --[no]fetch  | Fetch from origin before performing finish. |
| -r, --[no]rebase | Rebase before merging. |
| -p, --[no]preserve-merges | Preserve merges while rebasing. |
| --[no]push       | Push to origin after performing finish. |
| -k, --[no]keep   | Keep branch after performing finish. |
| --keepremote     | Keep the remote branch. |
| --keeplocal      | Keep the local branch. |
| -D, --[no]force_delete | Force delete bugfix branch after finish. |
| -S, --[no]squash | Squash bugfix during merge. |
| --no-ff          | Never fast-forward during the merge. |

### publish

Publish bugfix branch `name` on origin.
When `name` is omitted the current branch is used, but only if it's a bugfix branch.

```text
git flow bugfix publish [-h] [<name>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### track

Start tracking bugfix `name` that is shared on origin.

```text
git flow bugfix track [-h] <name>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### diff

Show all changes in `name` that are not in the base.

```text
git flow bugfix diff [-h] [<name|nameprefix>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### checkout

Switch to bugfix branch `name`.

```text
git flow bugfix checkout [-h] [<name|nameprefix>]
git flow bugfix co [-h] [<name|nameprefix>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### rebase

Rebase `name` on `base_branch`.

```text
git flow bugfix rebase [-h] [-i] [-p] [<name|nameprefix>]
```

|||
|---------------------------|--------------------------------------------------|
| -h, --help                | Show this help. |
| --showcommands            | Show git commands while executing them. |
| -i, --[no]interactive     | Do an interactive rebase. |
| -p, --[no]preserve-merges | Preserve merges. |

### pull

Pull bugfix `name` from `remote`.

> The command `git flow bugfix pull` will be deprecated per version 2.0.0.  
  Use `git flow bugfix track` instead.

```text
git flow bugfix pull [-h] <remote> [<name>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

### delete

Delete a given bugfix branch.

```text
git flow bugfix delete [-h] [-f] [-r] <name>
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |
| -f, --[no]force | Force deletion. |
| -r, --[no]remote | Delete remote branch. |

### rename

Rename a given bugfix branch.

```text
git flow bugfix rename [-h] <new_name> [<new_name>]
```

|||
|-----------------|------------------------------------------------------------|
| -h, --help      | Show this help. |
| --showcommands  | Show git commands while executing them. |

<div>&nbsp;</div>
