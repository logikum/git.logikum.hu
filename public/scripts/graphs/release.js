var BRANCH_COLOR = 'silver';
var MASTER_COLOR = 'purple';
var DEVELOP_COLOR = 'cornflowerblue';
var RELEASE_COLOR = 'green';
var SEPARATOR_COLOR = 'black';

// Helper for layer Branch
var textStart = new Point( 10, 55);
var lineMin = new Point( 0, 50 );
var lineMax = new Point( 680, 50 );
var lineStart = new Point( 150, 50 );
var lineEnd = new Point( 660, 50 );
var lineMulti = new Point( 0, 5 );
var lineBranch = new Point( 0, 50 );
var radius = 10;

drawGraph();

function drawGraph() {

  var lineCommit = new Point( 50, 0 );
  // Release commit points
  var user1_R2 = lineStart + lineBranch + lineCommit * 2;
  var user1_R3 = lineStart + lineBranch + lineCommit * 3;
  var user1_R4 = lineStart + lineBranch + lineCommit * 4;
  var user1_R5 = lineStart + lineBranch + lineCommit * 5;
  var user1_R6 = lineStart + lineBranch + lineCommit * 6;
  var user1_R7 = lineStart + lineBranch + lineCommit * 7;
  var user1_R8 = lineStart + lineBranch + lineCommit * 8;
  var origin_R4 = lineStart + lineBranch + lineCommit * 4 + lineBranch * 4;
  var origin_R6 = lineStart + lineBranch + lineCommit * 6 + lineBranch * 4;
  var user2_R4 = lineStart + lineBranch + lineCommit * 4 + lineBranch * 8;
  var user2_R5 = lineStart + lineBranch + lineCommit * 5 + lineBranch * 8;
  var user2_R6 = lineStart + lineBranch + lineCommit * 6 + lineBranch * 8;
  // Develop commit points
  var user1_D1 = lineStart + lineCommit;
  var user1_D9 = lineStart + lineCommit * 9;
  // Master commit points
  var user1_M9 = lineStart + lineBranch * 2 + lineCommit * 9;

  /* LAYER BRANCH */

  var layerBranch = new Layer();

  drawArea( 'user1', 0 );
  drawLine( lineMin + lineBranch * 3, lineMax + lineBranch * 3, SEPARATOR_COLOR, [5, 5] );
  drawArea( 'origin', 4 );
  drawLine( lineMin + lineBranch * 7, lineMax + lineBranch * 7, SEPARATOR_COLOR, [5, 5] );
  drawArea( 'user2', 8 );

  /* LAYER ARROW */

  var layerArrow = new Layer();

  drawArrow( user1_D1, user1_R2, RELEASE_COLOR );
  drawArrow( user1_R2, user1_R3, RELEASE_COLOR );
  drawArrow( user1_R3, user1_R4, RELEASE_COLOR );
  drawArrow( user1_R4, user1_R5, RELEASE_COLOR );
  drawArrow( user1_R5, user1_R6, RELEASE_COLOR );
  drawArrow( user1_R6, user1_R7, RELEASE_COLOR );
  drawArrow( user1_R7, user1_R8, RELEASE_COLOR );
  drawArrow( user1_R8, user1_D9, RELEASE_COLOR );
  drawArrow( user1_R8, user1_M9, RELEASE_COLOR );

  drawArrow( user1_R4, origin_R4, RELEASE_COLOR );
  drawArrow( origin_R4, user2_R4, RELEASE_COLOR );
  drawArrow( user2_R4, user2_R5, RELEASE_COLOR );
  drawArrow( user2_R5, user2_R6, RELEASE_COLOR );
  drawArrow( user2_R6, origin_R6, RELEASE_COLOR );
  drawArrow( origin_R6, user1_R6, RELEASE_COLOR );

  /* LAYER COMMIT */

  var layerCommit = new Layer();

  // Release commits
  drawCircle( user1_R2, RELEASE_COLOR );
  drawCircle( user1_R3, RELEASE_COLOR );
  drawCircle( user1_R4, RELEASE_COLOR );
  drawCircle( user1_R5, RELEASE_COLOR );
  drawCircle( user1_R6, RELEASE_COLOR );
  drawCircle( user1_R7, RELEASE_COLOR );
  drawCircle( user1_R8, RELEASE_COLOR );
  drawCircle( origin_R4, RELEASE_COLOR );
  drawCircle( origin_R6, RELEASE_COLOR );
  drawCircle( user2_R4, RELEASE_COLOR );
  drawCircle( user2_R5, RELEASE_COLOR );
  drawCircle( user2_R6, RELEASE_COLOR );
  // Develop commits
  drawCircle( user1_D1, DEVELOP_COLOR );
  drawCircle( user1_D9, DEVELOP_COLOR );
  // Master commits
  drawCircle( user1_M9, MASTER_COLOR, true );

  drawLabel( new Point(170,85), new Size(42,20), 'start' );
  drawLabel( new Point(595,85), new Size(48,20), 'finish' );
  drawLabel( new Point(285,180), new Size(60,20), 'publish' );
  drawLabel( new Point(465,180), new Size(35,20), 'pull' );
  drawLabel( new Point(300,430), new Size(45,20), 'track' );
  drawLabel( new Point(465,430), new Size(45,20), 'push' );
}

function drawArea( area, y ) {

  // Develop branch
  drawText( textStart + lineBranch * y, DEVELOP_COLOR, area + ': develop' );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, DEVELOP_COLOR );
  y++;
  // Release branches
  drawText( textStart + lineBranch * y, RELEASE_COLOR, area + ': releases' );
  drawLine( lineStart + lineBranch * y - lineMulti, lineEnd + lineBranch * y - lineMulti, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y + lineMulti, lineEnd + lineBranch * y + lineMulti, BRANCH_COLOR );
  // Master branch
  y++;
  drawText( textStart + lineBranch * y, MASTER_COLOR, area + ': master' );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, MASTER_COLOR );
}

function drawArrow( start, end, color ) {

  var path = new Path( [ start, end] );
  path.strokeColor = color;
  path.strokeWidth = 4;
  var vector = end - start;

  var w = 6;
  var h = 6;
  var pr = end + new Point( -w - radius, -h / 2);
  var arrowBg = new Path.Rectangle( pr, new Size( w, h ) );
  arrowBg.fillColor = 'white';
  arrowBg.rotate( vector.angle, end );

  w = 12;
  h = 10;
  var p0 = end + new Point( -w - radius, -h / 2);
  var p1 = new Point( p0.x, p0.y );
  var p2 = new Point( p0.x + w, p0.y + h / 2 );
  var p3 = new Point( p0.x, p0.y + h );
  var arrowFg = new Path( [ p1, p2, p3 ] );
  arrowFg.fillColor = color;
  arrowFg.rotate( vector.angle, end );
}

function drawCircle( center, color, tagged ) {

  var circle = new Path.Circle( center, radius );
  circle.strokeColor = color;
  circle.fillColor = 'white';
  circle.strokeWidth = 3;
  if (tagged) {
    var inner = new Path.Circle( center, radius / 2 );
    inner.fillColor = color;
  }
}

function drawLabel( start, size, content ) {

  var plate = new Path.Rectangle( start - new Point(5,16), size );
  plate.strokeWidth = 1;
  plate.strokeColor = 'black';
  plate.fillColor = 'lemonchiffon';
  var text = new PointText( start );
  text.content = content;
  text.fillColor = 'black';
  text.fontSize = 16;
}

function drawText( start, color, content ) {

  var text = new PointText( start );
  text.content = content;
  text.fillColor = color;
  text.fontSize = 16;
}

function drawLine( start, end, color, dash ) {

  var path = new Path();
  path.add( start );
  path.add( end );
  path.strokeColor = color;
  path.strokeWidth = 2;
  if (dash)
    path.dashArray = dash;
}
