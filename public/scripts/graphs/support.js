var BRANCH_COLOR = 'silver';
var MASTER_COLOR = 'purple';
var RELEASE_COLOR = 'green';
var FEATURE_COLOR = 'royalblue';
var HOTFIX_COLOR = 'red';
var SUPPORT_COLOR = 'goldenrod';

// Helper for layer Branch
var textStart = new Point( 10, 55);
var lineMin = new Point( 0, 50 );
var lineMax = new Point( 680, 50 );
var lineStart = new Point( 100, 50 );
var lineEnd = new Point( 660, 50 );
var lineMulti = new Point( 0, 5 );
var lineBranch = new Point( 0, 50 );
var pointStart = new Point( 80, 50 );
var radius = 10;

drawGraph();

function drawGraph() {

  // Commit circle radius
  var commitR = 10;
  var lineCommit = new Point( 50, 0 );
  // Master commit points
  var user1_M1 = pointStart + lineCommit + lineBranch * 4;
  // Support commit points
  var user1_S2 = pointStart + lineCommit * 2 + lineBranch * 2;
  var user1_S5 = pointStart + lineCommit * 5 + lineBranch * 2;
  var user1_S8 = pointStart + lineCommit * 8 + lineBranch * 2;
  var user1_S11 = pointStart + lineCommit * 11 + lineBranch * 2;
  // Hotfix commit points
  var user1_H3 = pointStart + lineCommit * 3 + lineBranch * 3;
  var user1_H4 = pointStart + lineCommit * 4 + lineBranch * 3;
  // Feature commit points
  var user1_F6 = pointStart + lineCommit * 6 + lineBranch;
  var user1_F7 = pointStart + lineCommit * 7 + lineBranch;
  // Releasee commit points
  var user1_R9 = pointStart + lineCommit * 9;
  var user1_R10 = pointStart + lineCommit * 10;

  /* LAYER BRANCH */

  var layerBranch = new Layer();

  // Release branches
  var y = 0;
  drawText( textStart + lineBranch * y, RELEASE_COLOR, 'releases' );
  drawLine( lineStart + lineBranch * y - lineMulti, lineEnd + lineBranch * y - lineMulti, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y + lineMulti, lineEnd + lineBranch * y + lineMulti, BRANCH_COLOR );
  // Feature branches
  y++;
  drawText( textStart + lineBranch * y, FEATURE_COLOR, 'features' );
  drawLine( lineStart + lineBranch * y - lineMulti, lineEnd + lineBranch * y - lineMulti, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y + lineMulti, lineEnd + lineBranch * y + lineMulti, BRANCH_COLOR );
  // Support branch
  y++;
  drawText( textStart + lineBranch * y, SUPPORT_COLOR, 'support' );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, SUPPORT_COLOR );
  // Hotfix branches
  y++;
  drawText( textStart + lineBranch * y, HOTFIX_COLOR, 'hotfixes' );
  drawLine( lineStart + lineBranch * y - lineMulti, lineEnd + lineBranch * y - lineMulti, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, BRANCH_COLOR );
  drawLine( lineStart + lineBranch * y + lineMulti, lineEnd + lineBranch * y + lineMulti, BRANCH_COLOR );
  // Master branch
  y++;
  drawText( textStart + lineBranch * y, MASTER_COLOR, 'master' );
  drawLine( lineStart + lineBranch * y, lineEnd + lineBranch * y, MASTER_COLOR );

  /* LAYER ARROW */

  var layerArrow = new Layer();

  drawArrow( user1_M1, user1_S2, SUPPORT_COLOR );
  // drawArrow( user1_S2, user1_S5, SUPPORT_COLOR );
  // drawArrow( user1_S5, user1_S8, SUPPORT_COLOR );
  // drawArrow( user1_S8, user1_S11, SUPPORT_COLOR );

  drawArrow( user1_S2, user1_H3, HOTFIX_COLOR );
  drawArrow( user1_H3, user1_H4, HOTFIX_COLOR );
  drawArrow( user1_H4, user1_S5, HOTFIX_COLOR );

  drawArrow( user1_S5, user1_F6, FEATURE_COLOR );
  drawArrow( user1_F6, user1_F7, FEATURE_COLOR );
  drawArrow( user1_F7, user1_S8, FEATURE_COLOR );

  drawArrow( user1_S8, user1_R9, RELEASE_COLOR );
  drawArrow( user1_R9, user1_R10, RELEASE_COLOR );
  drawArrow( user1_R10, user1_S11, RELEASE_COLOR );

  /* LAYER COMMIT */

  var layerCommit = new Layer();

  // Master commits
  drawCircle( user1_M1, MASTER_COLOR );
  // Release commits
  drawCircle( user1_R9, RELEASE_COLOR );
  drawCircle( user1_R10, RELEASE_COLOR );
  // Feature commits
  drawCircle( user1_F6, FEATURE_COLOR );
  drawCircle( user1_F7, FEATURE_COLOR );
  // Hotfix commits
  drawCircle( user1_H3, HOTFIX_COLOR );
  drawCircle( user1_H4, HOTFIX_COLOR );
  // Support commits
  drawCircle( user1_S2, SUPPORT_COLOR );
  drawCircle( user1_S5, SUPPORT_COLOR, true );
  drawCircle( user1_S8, SUPPORT_COLOR );
  drawCircle( user1_S11, SUPPORT_COLOR, true );

  drawLabel( new Point(155,240), new Size(42,20), 'start', 'peru' );
  drawLabel( new Point(225,175), new Size(42,20), 'start', HOTFIX_COLOR );
  drawLabel( new Point(325,185), new Size(48,20), 'finish', HOTFIX_COLOR );
  drawLabel( new Point(300,130), new Size(42,20), 'start', FEATURE_COLOR );
  drawLabel( new Point(400,140), new Size(48,20), 'finish', FEATURE_COLOR );
  drawLabel( new Point(470,80), new Size(42,20), 'start', RELEASE_COLOR );
  drawLabel( new Point(610,80), new Size(48,20), 'finish', RELEASE_COLOR );
}

function drawArrow( start, end, color ) {

  var path = new Path( [ start, end] );
  path.strokeColor = color;
  path.strokeWidth = 4;
  var vector = end - start;

  var w = 6;
  var h = 6;
  var pr = end + new Point( -w - radius, -h / 2);
  var arrowBg = new Path.Rectangle( pr, new Size( w, h ) );
  arrowBg.fillColor = 'white';
  arrowBg.rotate( vector.angle, end );

  w = 12;
  h = 10;
  var p0 = end + new Point( -w - radius, -h / 2);
  var p1 = new Point( p0.x, p0.y );
  var p2 = new Point( p0.x + w, p0.y + h / 2 );
  var p3 = new Point( p0.x, p0.y + h );
  var arrowFg = new Path( [ p1, p2, p3 ] );
  arrowFg.fillColor = color;
  arrowFg.rotate( vector.angle, end );
}

function drawCircle( center, color, tagged ) {

  var circle = new Path.Circle( center, radius );
  circle.strokeColor = color;
  circle.fillColor = 'white';
  circle.strokeWidth = 3;
  if (tagged) {
    var inner = new Path.Circle( center, radius / 2 );
    inner.fillColor = color;
  }
}

function drawLabel( start, size, content, color ) {

  var plate = new Path.Rectangle( start - new Point(5,16), size );
  plate.strokeWidth = 1;
  plate.strokeColor = 'black';
  plate.fillColor = 'lemonchiffon';
  var text = new PointText( start );
  text.content = content;
  text.fillColor = color;
  text.fontSize = 16;
}

function drawText( start, color, content ) {

  var text = new PointText( start );
  text.content = content;
  text.fillColor = color;
  text.fontSize = 16;
}

function drawLine( start, end, color, dash ) {

  var path = new Path();
  path.add( start );
  path.add( end );
  path.strokeColor = color;
  path.strokeWidth = 2;
  if (dash)
    path.dashArray = dash;
}
